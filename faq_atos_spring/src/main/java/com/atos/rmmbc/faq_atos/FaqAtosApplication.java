package com.atos.rmmbc.faq_atos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaqAtosApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaqAtosApplication.class, args);
	}

}
